ARG php_version=8

FROM php:${php_version}-alpine as base

ARG composer_version=2
ARG xdebug_version=""

ENV XDEBUG_VERSION="${xdebug_version}"
ENV COMPOSER_VERSION="${composer_version}"
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp

RUN set -eux; \
  apk add --no-cache --virtual .composer-rundeps \
    bash \
    curl \
    coreutils \
    git \
    graphicsmagick \
    make \
    mercurial \
    openssh-client \
    parallel \
    patch \
    rsync \
    subversion \
    tini \
    unzip \
    zip

RUN set -eux; \
  apk add --no-cache --virtual .build-deps \
    libzip-dev \
    zlib-dev \
    libpng-dev \
    libjpeg-turbo-dev \
    icu-dev \
  ; \
  docker-php-ext-configure gd \
    ; \
  docker-php-ext-install -j "$(nproc)" \
    gd \
    zip \
    mysqli \
    intl \
  ; \
  runDeps="$( \
    scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
      | tr ',' '\n' \
      | sort -u \
      | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )"; \
  apk add --no-cache --virtual .composer-phpext-rundeps $runDeps; \
  apk del .build-deps

RUN printf "# composer php cli ini settings\n\
date.timezone=UTC\n\
memory_limit=-1\n\
" > $PHP_INI_DIR/php-cli.ini

ADD https://getcomposer.org/installer /tmp/composer-setup.php

RUN php /tmp/composer-setup.php --quiet --install-dir=/usr/bin --filename=composer --$COMPOSER_VERSION \
    && rm -f /tmp/composer-setup.php

COPY docker-entrypoint.sh /docker-entrypoint.sh

WORKDIR /app

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["composer"]

FROM base as xdebug31
RUN apk add --no-cache linux-headers $PHPIZE_DEPS \
    && pecl install -f xdebug-3.1.6 \
    && apk del --purge linux-headers autoconf g++ make

FROM base as xdebug32
RUN apk add --no-cache linux-headers $PHPIZE_DEPS \
    && pecl install -f xdebug-3.2.2 \
    && apk del --purge linux-headers autoconf g++ make

FROM base as xdebug33
RUN apk add --no-cache linux-headers $PHPIZE_DEPS \
    && pecl install -f xdebug-3.3.2 \
    && apk del --purge linux-headers autoconf g++ make

FROM base as xdebug34
RUN apk add --no-cache linux-headers $PHPIZE_DEPS \
    && pecl install -f xdebug-3.4.1 \
    && apk del --purge linux-headers autoconf g++ make